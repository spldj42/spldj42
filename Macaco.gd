extends Node2D

var esta_en_isla = false
var en_isla = null
var is_rescued = false

func _ready():
	$AnimationPlayer.play("moving_hands")

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func rescue(rescued=false):
	is_rescued = rescued
	if rescued:
		$AnimationPlayer.play("happy")
	else:
		$AnimationPlayer.play("moving_hands")

func die():
	print("Macaco died")