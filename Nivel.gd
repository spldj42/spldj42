extends Node2D

var islas = []
var macacos = []

onready var macaco = preload("res://Macaco.tscn")
onready var you_lose = preload("res://YouLose.tscn")
onready var you_win = preload("res://YouWin.tscn")
var levels = [
	"res://Nivel-01.tscn",
	"res://Nivel-02.tscn",
	"res://Nivel-03.tscn",
]
export(int) var current_level = 0

var level_started = false
var ganado = false
var ganado_tiempo = 0

func _ready():
	_init_ppl()
	print(current_level)


func _input(event):
#
#	if !level_started:
#		_init_ppl()
#		level_started = true
	
	if event is InputEventMouseButton:
		if event.button_index == 1:
			$Player.destino = event.position
			
	if Input.is_action_just_pressed('restart_level'):
		restart_level()

#func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#_init_ppl()


func _process(delta):
	if ganado:
		ganado_tiempo += delta
		if ganado_tiempo >= 3:
			current_level = (current_level + 1) % 3
			get_tree().change_scene(levels[current_level])


	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass

func _init_ppl():
	
	for child in get_children():
		#if child.get_node("/root/Isla") != null:
			#print("tiene isla")
		if child.has_method("_shrink_isla"):
			islas.append(child)
			create_ppl(child)


func create_ppl(child):
	for i in child.initial_ppl:
		var m = macaco.instance()
		macacos.append(m)
		add_child(m)
		m.position = child.position
		m.esta_en_isla = true
		m.en_isla = child


func destroy_isla(isla_destroyed):
	print("isla destroyed: " + isla_destroyed.name)
	for ppl in macacos:
		if ppl.en_isla == isla_destroyed:
			ppl.get_parent().remove_child(ppl)
			var l = you_lose.instance()
			add_child(l)
			
func isla_get_macaco(isla, remove):
	for ppl in macacos:
		if ppl.en_isla == isla:
			if remove:
				ppl.en_isla = null
				if ppl.get_parent() != null:
					ppl.get_parent().remove_child(ppl)
			return ppl

func add_macaco_to_isla(m, isla, rescued=false):
	if isla.visible:
		m.esta_en_isla = true
		m.en_isla = isla
		m.position = isla.position
		m.rescue(rescued)
		add_child(m)
		if check_win():
			var w = you_win.instance()
			add_child(w)
			ganado = true
			ganado_tiempo = 0
	
func restart_level():
	get_tree().reload_current_scene()
	
func check_win():
	var win = true
	for m in macacos:
		if !m.is_rescued:
			win = false
			break
	return win
		