extends Area2D

export var speed = 100
export(Vector2) var destino
var macaco_onboard = null
var nivel
var prev_dir

const ABAJO = 0
const ABAJO_3_4_IZQUIERDA = 1
const IZQUIERDA = 2
const ARRIBA_3_4_IZQUIERDA = 3
const ARRIBA = 4
const ARRIBA_3_4_DERECHA = 5
const DERECHA = 6
const ABAJO_3_4_DERECHA = 7

func _ready():
	destino = position
	$heli.frame = ARRIBA
	$AnimationPlayer.play('girando')
	nivel = get_parent()

func _process(delta):

	var dir = Vector2(0, 0)

	if Input.is_action_pressed('derecha'):
		dir.x = 1
		destino = null
	if Input.is_action_pressed('izquierda'):
		dir.x = -1
		destino = null
	if Input.is_action_pressed('arriba'):
		dir.y = -1
		destino = null
	if Input.is_action_pressed('abajo'):
		dir.y = 1
		destino = null

	var next_position
	if not destino:
		dir = dir.normalized()
		next_position = position + delta * speed * dir
	else:
		dir = (destino - position).normalized()

		next_position = position + delta * speed * dir

		if sign(destino.x - next_position.x) != sign(destino.x - position.x):
			next_position.x = destino.x

		if sign(destino.y - next_position.y) != sign(destino.y - position.y):
			next_position.y = destino.y

	position = next_position

	if dir != Vector2(0, 0):
		var s = abs(dir.y)
		if s <= sin(deg2rad(22.5)) and \
		   s >= sin(deg2rad(-22.5)):
			$heli.frame = DERECHA if dir.x >= 0 else IZQUIERDA
		elif s <= sin(deg2rad(67.5)) and \
			 s >= sin(deg2rad(22.5)):
			if dir.y <= 0:
				$heli.frame = ARRIBA_3_4_DERECHA if dir.x >= 0 else ARRIBA_3_4_IZQUIERDA
			else:
				$heli.frame = ABAJO_3_4_DERECHA if dir.x >= 0 else ABAJO_3_4_IZQUIERDA
		else:
			$heli.frame = ARRIBA if dir.y <= 0 else ABAJO
	
	if Input.is_action_just_pressed('bajar_cuerda'):
		drop_rope()


func drop_rope():
	if macaco_onboard != null:
		drop_macaco_on_isla()
	else:
		get_macaco_from_isla()


func get_macaco_from_isla():
#	print('(get) overlapping_bodies: ', self.get_overlapping_bodies())
#	print('(get-areas) overlapping_bodies: ', self.get_overlapping_bodies())
	if macaco_onboard == null:
		for isla in self.get_overlapping_bodies():
			if isla.has_method("_shrink_isla"):
				var macaco = nivel.isla_get_macaco(isla, true)
				if macaco != null:
					macaco_onboard = macaco
				break


func drop_macaco_on_isla():
#	print('(drop) overlapping_bodies: ', self.get_overlapping_bodies())
	for isla in self.get_overlapping_bodies():
		if isla.has_method("_shrink_isla"):
			var macaco = nivel.isla_get_macaco(isla, false)
			if macaco == null:
				nivel.add_macaco_to_isla(macaco_onboard,
										 isla,
										 isla.is_puerto())
				macaco_onboard = null
			break
