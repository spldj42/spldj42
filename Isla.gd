extends StaticBody2D

# class member variables go here, for example:
export var ttl = 20
export var ttl_delay = 0
export var initial_ppl = 0
export var ttl_offset = 0.8
export var type_puerto = false
export var frame_selected = 0
var isla_is_alive = true
var init_ttl
var init_scale
var current_scale
var t

onready var macaco = preload("res://Macaco.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	if type_puerto:
		$Iceberg.visible = false
		$Helipuerto.visible = true
	else:
		$Iceberg.frame = frame_selected % ($Iceberg.vframes * $Iceberg.hframes)
	t = transform
	init_ttl = ttl
	init_scale = t.get_scale()
	if init_ttl < 0:
		set_process(false)


func _process(delta):
	if ttl_delay > 0:
		ttl_delay -= delta
	else:
		_shrink_isla(delta)


func _shrink_isla(delta):
	if ttl > ttl_offset:
		ttl -= delta
		current_scale = ttl * init_scale.x / init_ttl

		scale = Vector2(current_scale, current_scale)
	else:
		if isla_is_alive:
			isla_is_alive = false
			_kill_ppl()
			self.visible = false

#		get_parent().remove_child(self)
func is_puerto():
	return type_puerto


func _kill_ppl():
	#for ppl in ppl_in_isla:
	get_parent().destroy_isla(self)

